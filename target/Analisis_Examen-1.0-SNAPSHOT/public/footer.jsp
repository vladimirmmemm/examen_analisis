        <footer role="contentinfo" id="page-footer">
        <div class="container-fluid">
            <div class="footerlinks row-fluid">
                <span class="helplink"></span>
                                <div class="footnote span12"><div class="row">
    	<div class="span5" style="text-align: center;">
        <a href="https://www.ucr.ac.cr" target="_blank">
            <img src="/theme/essential/pix/metics/firma-horizontal-dos-lineas-blanco.png" alt="Logo de UCR" style="height: 80px;">
        </a>
        <a href="http://vd.ucr.ac.cr/" target="_blank">
            <img src="/theme/essential/pix/metics/firma-vd-blanco-horizontal.png" alt="Logo de VD" style="height: 50px;">
        </a>
        <a href="https://metics.ucr.ac.cr/" target="_blank">
            <img src="/theme/essential/pix/metics/firma-metics-blanco-horizontal.png" alt="Logo de METICS" style="height: 35px;">
        </a>
    </div>
    <div class="span3" style="text-align: left;"><br><p>Para consultas técnicas sobre Mediación Virtual llame al 2511-4846 ó 2511-5017 o escríbanos a soporte.metics@ucr.ac.cr</p></div>
	</div></div>            </div>
            <div class="footerperformance row-fluid">
                <a href="https://download.moodle.org/mobile?version=2018051704.05&amp;lang=es_mx&amp;iosappid=633359593&amp;androidappid=com.moodle.moodlemobile">Obtener la App Mobile</a>            </div>
        </div>
    </footer>
