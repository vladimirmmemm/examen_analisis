//Variables Globales
$listaPedidos = {};
$listaClientes = {};
$listaProductos = {};
tablaPedidos = null;

$total = 0;
$detalle = "";

//Funcion de inicio
$(document).ready(function (){
    cargarNavbar();
    obtenerCliente();
    obtenerProductos();
    obtenerPedidos();
    
});

//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link' href='./index.jsp'>Inicio</a>"+
                "<a class='nav-item nav-link' href='./producto.jsp'>Productos<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./cliente.jsp'>Clientes</a>"+
               "<a class='nav-item nav-link active' href='./pedido.jsp'>Pedidos</a>";
    $('.navbar-nav').html(html);
}

//Funcion obtener lista productos
function obtenerPedidos(){  
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "./consultarPedido");
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            $listaPedidos = JSON.parse(this.responseText);
            //console.log($listaPedidos);
            inicializarTabla();
        }
    };
}

function obtenerCliente(){  
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "./consultarCliente");
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {;
            $listaClientes = JSON.parse(this.responseText);
        }
    };
}

function obtenerProductos(){  
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "./consultarProducto");
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {;
            $listaProductos = JSON.parse(this.responseText);
        }
    };
}

function insertarProductoP(){
    $producto = $("#producto").val();
    $cant = $("#cantidad").val();
    
    $posicion = buscarProductoP($producto);
    
    $("#tablePFC").append(
        '<tr>'+
            '<td>'+$listaProductos[$posicion].nombre_Producto+'</td>'+
            '<td>'+$cant+'</td>'+
            '<td>'+$listaProductos[$posicion].precio_Producto+'</td>'+
        '</tr>'
    );
    
    $detalle += $listaProductos[$posicion].id_Producto+"-"+$cant+",";
    
    $total += (parseInt($cant)*parseInt($listaProductos[$posicion].precio_Producto));
    $("#totalFC").text($total);
    
}

function buscarProductoP(codigoProducto){

    $posicionProducto = 0;

    for(var i = 0; i < $listaProductos.length; i++){
        //alert($listaProductos[i].id_Producto+", "+codigoProducto)
        if( $listaProductos[i].id_Producto == codigoProducto){
            $posicionProducto = i;
            i = $listaProductos.length;
        }    
    }
    return $posicionProducto;
}

//Funcion elimiar el producto
function eliminarProducto(Codigo_P){

    $posicion = buscarProducto(Codigo_P);

    swal({
        title: "Esta Seguro de Eliminar el pedido de " + $listaPedidos[$posicion].cliente + "?",
        text: "Al eliminar este producto no se puede recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, Eliminarlo!",
        cancelButtonClass: "btn btn-secondary",
        cancelButtonText: "No, Cancelar!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", "./eliminarPedido?id="+$listaPedidos[$posicion].id);
            xhttp.send();
            xhttp.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {;

                    if(this.responseText != "false"){
                        alertify.success('EL producto fue eliminado!!');
                        obtenerPedidos();
                        $('#modalGeneralProducto').modal('hiden');
                    }else{
                        alertify.error('No se pudo eliminar el producto!!');
                    }

                }
            };
      } else {
        alertify.success('Su producto esta a salvo!!');
      }
    });
}

//Funcion insertar producto
function insertarPedido(){

    cedulaCliente = $('#cedula').val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "./insertarPedido?cedula="+cedulaCliente+"&detalle="+$detalle);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {;

            if(this.responseText != "false"){
                alertify.success('Pedido enviado correctamente');
                obtenerPedidos();
                $('#modalGeneralPedido').modal('hide');
                $detalle = "";
            }else{
                alertify.error('ERROR EN EL PEDIDO');
            }
            
        }
    };
}


//Funcion abrir modal Registrar
function abrirModalRegistrar(){

    $('#tituloModal').text('Registrar pedido');
    $('#btnAccion').text('Agregar');
    $('#btnAccion').attr('onclick', 'insertarPedido()');
    
    $cliente = $("#cedula");
    
    for(var i=0; i<$listaClientes.length; i++){
        $cliente.append(
            "<option value='"+$listaClientes[i].cedula+"'>"+$listaClientes[i].nombre+"</option>"
        );
    }
    $producto = $("#producto");
    for(var i=0; i<$listaProductos.length; i++){
        $producto.append(
            "<option value='"+$listaProductos[i].id_Producto+"'>"+$listaProductos[i].nombre_Producto+"</option>"
        );
    }

    limpiarCampos();

    $('#modalGeneralPedido').modal('show');
}

//Funcion dejar los campos vacios
function limpiarCampos(){

    $('#Nombre_P').val("");
    $('#Descripcion_P').val("");
    $('#Precio_P').val("");
    $('#input-b6').fileinput('clear');

}

//Funcion abrir modal editar
function abrirModalEditar(codigoProducto){
       
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "./consultarDetalle?id="+codigoProducto);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            
            $("#detalleFC").append(this.responseText);
            $('#modalDetalleFC').modal('show');
        }
    };
    
}

//Funcion obtener el objeto producto
function buscarProducto(codigoProducto){

    $posicionProducto = 0;

    for(var i = 0; i < $listaPedidos.length; i++){
        //alert($listaProductos[i].id_Producto+", "+codigoProducto)
        if( $listaPedidos[i].id == codigoProducto){
            $posicionProducto = i;
            i = $listaPedidos.length;
        }    
    }
    return $posicionProducto;
}

//Se crea la tabla 
function inicializarTabla() { 

    if(tablaPedidos != null){
        tablaPedidos.clear().destroy();
        tablaPedidos = null
    }

    tablaPedidos = $('#tbPedidos').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaPedidos,
        columns: [
            { title: "ID", data: "id" },
            { title: "Nombre cliente", data: "cliente"  },
            { title: "Fecha del pedido", data: "fecha"  },
            { title: "Editar" },
            { title: "Eliminar" },
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [ 
        {
            "targets": 3, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {                                                                                                                                                                                     
                 return "<button id='editar' type='button' data-toggle='tooltip' data-placement='top' title='Editar selección' class='cont-icono btn btn-outline-succes' disabled><i class='fas fa-edit' onclick='abrirModalEditar(" + data.id + ")'></i></button>";
            }
        },
        {
            "targets": 4, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                 return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='far fa-trash-alt' onclick='eliminarProducto("+data.id+")'></i></button>"
            }
        }
        ],
        "order": [[0, "desc"]],
        "autoWidth": false,
        "preDrawCallback": function(settings) {
                $("#tbProductos tbody tr").removeClass("seleccionado");
                $("#tbProductos tbody tr td").removeClass("selected");
            },
        "drawCallback": function(settings) {
            $("#eliminar").prop("disabled", true);
            $("#tbProductos tbody tr").removeClass("selected");
            $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        }
    });
}

function validarNumero(evt) {
    
    var code = evt.which ? evt.which : evt.keyCode;
    if (code == 8) {
        //backspace
        return true;
    } else if (code >= 48 && code <= 57) {
        //is a number
        return true;
    } else {
        return false;
    }
}


