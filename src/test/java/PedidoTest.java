/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cr.ac.ucr.dominio.Detalle;
import cr.ac.ucr.dominio.Pedido;
import cr.ac.ucr.dominio.Producto;
import cr.ac.ucr.negocio.LogicaPedido;
import java.util.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;

public class PedidoTest {
    
    private static LogicaPedido logP;
    private static Pedido pedido;
    
    private static LinkedList<Detalle> lista = new LinkedList<Detalle>();
    
    public PedidoTest() {
        logP = new LogicaPedido();
        
        pedido = new Pedido(0, "402410080", "", lista);
    }
    
    @Test
    public void testInsertarTrue(){
        lista.add(new Detalle(new Producto(4, "", 0, 0), 1));
        assertTrue(logP.insertarPedido(pedido));
    }
    @Test
    public void testInsertarFalse(){
        lista.add(new Detalle(new Producto(5, "", 0, 0), 1));
        assertFalse(!logP.insertarPedido(pedido));
    }

    @Test
    public void testEliminarTrue(){
        assertTrue(logP.eliminarPedido(1));
    }
    @Test
    public void testEliminarFalse(){
        assertFalse(!logP.eliminarPedido(1));
    }
    
    @Test
    public void testListaEmpy(){
        assertTrue(!logP.getListaPedidos().isEmpty());
    }
    @Test
    public void testListaNoEmpy(){
        assertFalse(logP.getListaPedidos().isEmpty());
    }
    
    @Test
    public void testDetalleEmpy(){
        assertTrue(logP.getListaDetalles(1) == "");
    }
    @Test
    public void testDetalleNoEmpy(){
        assertFalse(logP.getListaDetalles(1) != "");
    }
    
}
