  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cr.ac.ucr.dominio.Cliente;
import cr.ac.ucr.negocio.LogicaCliente;
import static org.junit.Assert.*;
import org.junit.Test;

public class ClienteTest {
    
    private static LogicaCliente logC;
    private static Cliente cliente;
    
    public ClienteTest() {
        logC = new LogicaCliente();
        cliente = new Cliente("402410082", "Cristofer", "12345678", "cris@gmail.com");
    }
    
    @Test
    public void testInsertarTrue(){
        assertTrue(logC.insertarCliente(new Cliente("402410083", "Cristofer", "12345678", "cris@gmail.com")));
    }
    @Test
    public void testInsertarFalse(){
        assertFalse(!logC.insertarCliente(new Cliente("402410084", "Cristofer", "12345678", "cris@gmail.com")));
    }
    
    @Test
    public void testModificarTrue(){
        assertTrue(logC.modificarCliente(cliente));
    }
    @Test
    public void testModificarFalse(){
        assertFalse(!logC.modificarCliente(cliente));
    }

    @Test
    public void testEliminarTrue(){
        assertTrue(logC.eliminarCliente("402410083"));
    }
    @Test
    public void testEliminarFalse(){
        assertFalse(!logC.eliminarCliente("402410084"));
    }
    
    @Test
    public void testListaEmpy(){
        assertTrue(!logC.getListaClientes().isEmpty());
    }
    @Test
    public void testListaNoEmpy(){
        assertFalse(logC.getListaClientes().isEmpty());
    }
}
