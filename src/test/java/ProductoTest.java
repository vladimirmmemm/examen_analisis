/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cr.ac.ucr.dominio.Producto;
import cr.ac.ucr.negocio.LogicaProducto;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author crisc
 */
public class ProductoTest {
    
    private static LogicaProducto logP;
    private static Producto pro;
    
    public ProductoTest() {
        logP = new LogicaProducto();
        pro = new Producto(1, "", 10, 1200);
    }
    
    @Test
    public void testInsertarTrue(){
        assertTrue(logP.insertarProducto(pro));
    }
    @Test
    public void testInsertarFalse(){
        assertFalse(!logP.insertarProducto(pro));
    }
    
    @Test
    public void testModificarTrue(){
        assertTrue(logP.modificarProducto(pro));
    }
    @Test
    public void testModificarFalse(){
        assertFalse(!logP.modificarProducto(pro));
    }

    /*@Test
    public void testEliminarTrue(){
        assertTrue(!logP.eliminarProducto(1));
    }
    @Test
    public void testEliminarFalse(){
        assertFalse(logP.eliminarProducto(1));
    }*/
    
    @Test
    public void testListaEmpy(){
        assertTrue(!logP.getListaProductos().isEmpty());
    }
    @Test
    public void testListaNoEmpy(){
        assertFalse(logP.getListaProductos().isEmpty());
    }
    
}
