<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pedido</title>
        
        <%@include file="public/head.jsp" %>
        
        <script src="script/pedido.js"></script>
    </head>
    <body>
    <%@include file="public/menu.jsp" %>
        
    <div class="container">
	
	<a id="agregarFV" class="cont-icono btn btn-outline-primary float-left" data-toggle="tooltip" data-placement="top" title="Crear noticia" onclick="abrirModalRegistrar()"><i class="far fa-plus-square" ></i></a>

	<table id="tbPedidos" className="display"></table>

        
        
        
        
	<div class="modal fade" id="modalGeneralPedido" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModalFC"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="contenidoModalFC">

          	<div class="form-group">
              <label>Cedula cliente: </label>
              <select class="form-control" id="cedula"></select>
            </div>

            <div class="form-group">
              <label>Productos:</label>
              <div class="row">
              	<div class="col-md-6">
              		<select class="form-control" id="producto"></select>
              	</div>
              	<div class="col-md-3">
              		<input type="text" class="form-control" id="cantidad" value="1" onkeypress="return validarNumero(event);">
              	</div>
              	<div class="col-md-2">
              		<button type="button" class="btn btn-primary" onclick="insertarProductoP()">Agregar</button>
              	</div>
              </div>
            </div>
            <br>
            <div class="form-group">
              <table class="table">
              	<thead>
              		<tr>
              			<th>Nombre Producto</th>
              			<th>Cantidad</th>
              			<th>Precio x U</th>
              		</tr>
              	</thead>
              	<tbody id="tablePFC"></tbody>
              </table>
            </div>

          </div>
          <div class="modal-footer">
          	<label>Total: </label><label id="totalFC">0 </label>
          	
            <button type="button" class="btn btn-primary" id="btnAccion"></button>
          </div>
        </div>
      </div>
    </div>
        
        
        
        
        
        
        
        
        

    <div class="modal fade" id="modalDetalleFC" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModalFC"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="contenidoDetalleFC">
          	<table class="table">
          		<thead>
          			<tr>
          				<th>Nombre Producto</th>
          				<th>Cantidad</th>
              			<th>Precio x U</th>
          			</tr>
          		</thead>
          		<tbody id="detalleFC"></tbody>
          	</table>
          </div>
        </div>
      </div>
    </div>

</div>
    </body>
</html>
