<%-- 
    Document   : cliente
    Created on : Nov 25, 2019, 9:21:59 AM
    Author     : crisc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cliente</title>
        
        <%@include file="public/head.jsp" %>
        
        <script src="script/cliente.js" type="text/javascript"></script>
    </head>
    <body>
        <%@include file="public/menu.jsp" %>
        
        <div class="container marketing">
      <div class="mb-3">
        <button id="selTodo" type="button" data-toggle="tooltip" data-placement="top" title="Seleccionar todo" class="cont-icono btn btn-outline-info mr-2"><i class="far fa-check-square"></i></button>
        <button id="desTodo" type="button" data-toggle="tooltip" data-placement="top" title="Cancelar selección" class="cont-icono btn btn-outline-info mr-2"><i class="far fa-square"></i></button>
        <button id="eliminar" type="button" data-toggle="tooltip" data-placement="top" title="Eliminar selección" class="cont-icono btn btn-outline-danger" disabled><i class="far fa-trash-alt"></i></button>
        <a id="agregar" class="cont-icono btn btn-outline-primary float-right" data-toggle="tooltip" data-placement="top" title="Crear noticia" onclick="abrirModalRegistrar()"><i class="far fa-plus-square" ></i></a>
      </div>
    </div>
    <div class="container">
      <div class="mb-3">
        <table id="tbProductos" className="display">
          <thead>
                    <tr>
                    </tr>
                </thead>
                <tbody>
                </tbody>
        </table>
      </div>
    </div>

    <div class="modal fade" id="modalGeneralProducto" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-sm-11">
                            <h3 class="modal-title" id="tituloModal"></h3>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body" id="contenidoModal">
                        <div class="form-group">
                            <label for="nombre">Cédula: </label>
                            <input type="text" class="form-control" id="cedulaC" maxlength="9" onkeypress="return validarNumero(event);">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre: </label>
                            <input type="text" class="form-control" id="nombreC" onkeypress="validarTexto()" maxlength="40">
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Télefono:</label>
                            <input type="text" class="form-control" id="telC" onkeypress="return validarNumero(event);" maxlength="8">
                        </div>
                        <div class="form-group">
                            <label for="precio">Correo:</label>
                            <input type="text" class="form-control" id="correoC" maxlength="60">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnAccion"></button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
