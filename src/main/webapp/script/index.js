//Funcion de inicio
$(document).ready(function () {
    cargarNavbar();
});

//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link active' href='./index.jsp'>Inicio<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./producto.jsp'>Productos</a>"+
                "<a class='nav-item nav-link' href='./cliente.jsp'>Clientes</a>"+
                "<a class='nav-item nav-link' href='./pedido.jsp'>Pedidos</a>";
    $('.navbar-nav').html(html);

}


