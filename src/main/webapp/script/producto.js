//Variables Globales
$listaProductos = {};
$tablaProductos = null;

//Funcion de inicio
$(document).ready(function () {
    cargarNavbar();
    obtenerProductos();
});

//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link' href='./index.jsp'>Inicio</a>"+
                "<a class='nav-item nav-link active' href='./producto.jsp'>Productos<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./cliente.jsp'>Clientes</a>"+
               "<a class='nav-item nav-link' href='./pedido.jsp'>Pedidos</a>";
    $('.navbar-nav').html(html);
}

//Funcion obtener lista productos
function obtenerProductos(){  
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "./consultarProducto");
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {;
            $listaProductos = JSON.parse(this.responseText);
            console.log($listaProductos);
            inicializarTabla();
        }
    };
}

//Funcion elimiar el producto
function eliminarProducto(Codigo_P){

    $posicion = buscarProducto(Codigo_P);

    swal({
        title: "Esta Seguro de Eliminar el Producto " + $listaProductos[$posicion].nombre_Producto + "?",
        text: "Al eliminar este producto no se puede recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, Eliminarlo!",
        cancelButtonClass: "btn btn-secondary",
        cancelButtonText: "No, Cancelar!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", "./eliminarProducto?id="+$listaProductos[$posicion].id_Producto);
            xhttp.send();
            xhttp.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {;

                    if(this.responseText != "false"){
                        alertify.success('EL producto fue eliminado!!');
                        obtenerProductos();
                        $('#modalGeneralProducto').modal('hiden');
                    }else{
                        alertify.error('No se pudo eliminar el producto!!');
                    }

                }
            };
      } else {
        alertify.success('Su producto esta a salvo!!');
      }
    });
}

//Funcion insertar producto
function insertarProducto(){

    $Nombre_P = $('#Nombre_P').val();
    $Descripcion_P = $('#Descripcion_P').val();
    $Precio_P = $('#Precio_P').val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "./insertarProducto?nombre="+$Nombre_P+
        "&cantidad="+$Descripcion_P+"&precio="+$Precio_P);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {;

            if(this.responseText != "false"){
                alertify.success('Su producto se agrego correctamente!!');
                obtenerProductos();
                $('#modalGeneralProducto').modal('hide');
            }else{
                alertify.error('No se pudo agregar el producto!!');
            }
            
        }
    };
}


//Funcion editar producto
function editarProducto(codigoProducto){

    $Nombre_P = $('#Nombre_P').val();
    $Descripcion_P = $('#Descripcion_P').val();
    $Precio_P = $('#Precio_P').val();
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "./modificarProducto?id="+codigoProducto+"&nombre="+$Nombre_P+
        "&cantidad="+$Descripcion_P+"&precio="+$Precio_P);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {;
            
            if(this.responseText != "false"){
                alertify.success('Su producto ' + $Nombre_P + ' se edito correctamente');
                obtenerProductos();
                $('#modalGeneralProducto').modal('hide');
            }else{
                alertify.error('Ocurrio un error al Editar Producto');
            }
            
        }
    };

}

//Funcion abrir modal Registrar
function abrirModalRegistrar(){

    $('#tituloModal').text('Registrar Producto');
    $('#btnAccion').text('Agregar');
    $('#btnAccion').attr('onclick', 'insertarProducto()');

    limpiarCampos();

    $('#modalGeneralProducto').modal('show');
}

//Funcion dejar los campos vacios
function limpiarCampos(){

    $('#Nombre_P').val("");
    $('#Descripcion_P').val("");
    $('#Precio_P').val("");
    $('#input-b6').fileinput('clear');

}

//Funcion abrir modal editar
function abrirModalEditar(codigoProducto){

    $('#tituloModal').text('Editar Producto');
    $('#btnAccion').text('Guardar cambios');
    $('#btnAccion').attr('onclick', 'editarProducto('+codigoProducto+')');

    limpiarCampos();

    $posicion = buscarProducto(codigoProducto);

    $('#Nombre_P').val($listaProductos[$posicion].nombre_Producto);
    $('#Descripcion_P').val($listaProductos[$posicion].cantidad_Producto);
    $('#Precio_P').val($listaProductos[$posicion].precio_Producto);

    $('#modalGeneralProducto').modal('show');
}

//Funcion obtener el objeto producto
function buscarProducto(codigoProducto){

    $posicionProducto = 0;

    for(var i = 0; i < $listaProductos.length; i++){
        //alert($listaProductos[i].id_Producto+", "+codigoProducto)
        if( $listaProductos[i].id_Producto == codigoProducto){
            $posicionProducto = i;
            i = $listaProductos.length;
        }    
    }
    return $posicionProducto;
}

//Se crea la tabla 
function inicializarTabla() { 

    if($tablaProductos != null){
        $tablaProductos.clear().destroy();
        $tablaProductos = null
    }

    $tablaProductos = $('#tbProductos').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaProductos,
        columns: [
            { title: "ID", data: "id_Producto" },
            { title: "Nombre", data: "nombre_Producto"  },
            { title: "Cantidad", data: "cantidad_Producto"  },
            { title: "Precio", data: "precio_Producto"  },
            { title: "Editar" },
            { title: "Eliminar" },
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [ 
        {
            "targets": 4, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {                                                                                                                                                                                     
                 return "<button id='editar' type='button' data-toggle='tooltip' data-placement='top' title='Editar selección' class='cont-icono btn btn-outline-succes' disabled><i class='fas fa-edit' onclick='abrirModalEditar(" + data.id_Producto + ")'></i></button>";
            }
        },
        {
            "targets": 5, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                 return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='far fa-trash-alt' onclick='eliminarProducto("+data.id_Producto+")'></i></button>"
            }
        }
        ],
        "order": [[0, "desc"]],
        "autoWidth": false,
        "preDrawCallback": function(settings) {
                $("#tbProductos tbody tr").removeClass("seleccionado");
                $("#tbProductos tbody tr td").removeClass("selected");
            },
        "drawCallback": function(settings) {
            $("#eliminar").prop("disabled", true);
            $("#tbProductos tbody tr").removeClass("selected");
            $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        }
    });
}

function validarNumero(evt) {
    
    var code = evt.which ? evt.which : evt.keyCode;
    if (code == 8) {
        //backspace
        return true;
    } else if (code >= 48 && code <= 57) {
        //is a number
        return true;
    } else {
        return false;
    }
}

function validarTexto() {
    if ((event.keyCode != 32) && (event.keyCode < 65))
     event.returnValue = false;
   }


