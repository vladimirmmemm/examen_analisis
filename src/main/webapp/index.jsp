<%-- 
    Document   : index
    Created on : Nov 24, 2019, 9:03:51 AM
    Author     : crisc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inicio</title>
        
         <%@include file="public/head.jsp" %>
        
        <script src="script/index.js" type="text/javascript"></script>
    </head>
    <body>
        
        <%@include file="public/menu.jsp" %>
        
        <div class="jumbotron text-center">
            <h1>Curso Análisis</h1>
        </div>
    </body>
</html>
