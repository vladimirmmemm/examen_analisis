package cr.ac.ucr.dominio;

import java.util.LinkedList;

public class Pedido {
    private int id;
    private String cliente;
    private String fecha;
    private LinkedList<Detalle> detalle;

    public Pedido(int id, String cliente, String fecha, LinkedList<Detalle> detalle) {
        this.id = id;
        this.cliente = cliente;
        this.fecha = fecha;
        this.detalle = detalle;
    }
    public Pedido() {
        this.id = 0;
        this.cliente = "";
        this.fecha = "";
        this.detalle = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public LinkedList<Detalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(LinkedList<Detalle> detalle) {
        this.detalle = detalle;
    }
}
