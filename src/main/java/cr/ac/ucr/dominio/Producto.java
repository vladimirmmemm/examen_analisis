package cr.ac.ucr.dominio;

public class Producto {
    private int id_Producto;
    private String nombre_Producto;
    private int cantidad_Producto;
    private int precio_Producto;

    public Producto(int id_Producto, String nombre_Producto, int cantidad_Producto, int precio_Producto) {
        this.id_Producto = id_Producto;
        this.nombre_Producto = nombre_Producto;
        this.cantidad_Producto = cantidad_Producto;
        this.precio_Producto = precio_Producto;
    }

    public Producto() {
        this.id_Producto = 0;
        this.nombre_Producto = "";
        this.cantidad_Producto = 0;
        this.precio_Producto = 0;
    }

    public int getId_Producto() {
        return id_Producto;
    }

    public void setId_Producto(int id_Producto) {
        this.id_Producto = id_Producto;
    }

    public String getNombre_Producto() {
        return nombre_Producto;
    }

    public void setNombre_Producto(String nombre_Producto) {
        this.nombre_Producto = nombre_Producto;
    }

    public int getCantidad_Producto() {
        return cantidad_Producto;
    }

    public void setCantidad_Producto(int cantidad_Producto) {
        this.cantidad_Producto = cantidad_Producto;
    }

    public int getPrecio_Producto() {
        return precio_Producto;
    }

    public void setPrecio_Producto(int precio_Producto) {
        this.precio_Producto = precio_Producto;
    }

    @Override
    public String toString() {
        return "Producto{" + "id_Producto=" + id_Producto + ", nombre_Producto=" + nombre_Producto + ", cantidad_Producto=" + cantidad_Producto + ", precio_Producto=" + precio_Producto + '}';
    }
}
