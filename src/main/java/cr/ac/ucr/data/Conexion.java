package cr.ac.ucr.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexion {
    
    private static final String url = "jdbc:mysql://107.180.4.72:3306/sodaonline?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String user = "sodaonline";
    private static final String pass = "Ond3#9HwzS";
    
    private static Connection conn = null;

    public Conexion() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConection(){
        try {
           conn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException ex) {
            System.out.println("NO SE PUDO CONECTAR AL PRIMER SERVIDOR");
            System.out.println(ex.getMessage());
        }
        return conn;
    }
    
    public void closeConection(){
        try {
            conn.close();
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
