package cr.ac.ucr.data;

import cr.ac.ucr.dominio.Producto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class dataProducto extends Conexion{
    
    private static Connection con;
    private static PreparedStatement statement;
    
    public boolean insertarProducto(Producto pro){
        boolean inserto = false;
        con = super.getConection();
        String sql = " CALL ex_insertarProducto(?, ?, ?);";
        
        try {
            con.setAutoCommit(false);
            
            statement = con.prepareStatement(sql);
            statement.setString(1, pro.getNombre_Producto());
            statement.setInt(2, pro.getCantidad_Producto());
            statement.setInt(3, pro.getPrecio_Producto());
            statement.executeQuery();
            
            con.commit();
            inserto = true;
            
        } catch (SQLException ex) {
            try {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }finally{
            super.closeConection();
        }
        
        return inserto;
    }
    
    public boolean modificarProducto(Producto pro){
        boolean inserto = false;
        con = super.getConection();
        String sql = " CALL ex_modificarProducto(?, ?, ?, ?);";
        
        try {
            con.setAutoCommit(false);
            
            statement = con.prepareStatement(sql);
            statement.setInt(1, pro.getId_Producto());
            statement.setString(2, pro.getNombre_Producto());
            statement.setInt(3, pro.getCantidad_Producto());
            statement.setInt(4, pro.getPrecio_Producto());
            statement.executeQuery();
            
            con.commit();
            inserto = true;
            
        } catch (SQLException ex) {
            try {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }finally{
            super.closeConection();
        }
        
        return inserto;
    }
    
    public boolean eliminarProducto(int id){
        boolean elimino = false;
        con = super.getConection();
        String sql = " CALL ex_eliminarProducto(?);";
        
        try {
            con.setAutoCommit(false);
            
            statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeQuery();
            
            con.commit();
            elimino = true;
            
        } catch (SQLException ex) {
            try {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }finally{
            super.closeConection();
        }
        
        return elimino;
    }
    
    public LinkedList<Producto> getProductos(){
        LinkedList<Producto> list = new LinkedList<>();
        
        con = super.getConection();
        String sql = " CALL ex_selectProducto();";
        
        try {
            
            statement = con.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Producto pro = new Producto();
                pro.setId_Producto(result.getInt(1));
                pro.setNombre_Producto(result.getString(2));
                pro.setCantidad_Producto(result.getInt(3));
                pro.setPrecio_Producto(result.getInt(4));
                
                list.add(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            //super.closeConection();
        }
        
        return list;
    }
}
