package cr.ac.ucr.data;

import cr.ac.ucr.dominio.Cliente;
import cr.ac.ucr.dominio.Detalle;
import cr.ac.ucr.dominio.Pedido;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class dataPedido extends Conexion{
    
    private static Connection con;
    private static PreparedStatement statement;
    
    public boolean insertarPedido(Pedido pedido){
        boolean inserto = false;
        String sql = "CALL ex_insertarPedido(?)";  
        String sql2 = "CALL ex_insertarDetalle(?,?,?)";
        
        con = super.getConection();
        
        try {
            con.setAutoCommit(false);
            int id = 0;
            
            // ----- insertar Pedido ----------
            statement = con.prepareStatement(sql);
            statement.setString(1, pedido.getCliente());
            ResultSet result = statement.executeQuery();
            if(result.next()){
                id = result.getInt(1);
            }
            
            // ----- insertar Detalle ---------
            statement = con.prepareStatement(sql2);
            for(Detalle detalle: pedido.getDetalle()){
                statement.setInt(1, id);
                statement.setInt(2, detalle.getProducto().getId_Producto());
                statement.setInt(3, detalle.getCantidad());
                statement.executeQuery();
            }
            
            con.commit();
            inserto = true;
        } catch (SQLException ex) {
            try {
                Logger.getLogger(dataPedido.class.getName()).log(Level.SEVERE, null, ex);
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(dataPedido.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }finally{
            super.closeConection();
        }
        
        return inserto;
    }
    
    public boolean eliminarPedidos(int id){
        boolean elimino = false;
        con = super.getConection();
        String sql = " CALL ex_eliminarPedido(?);";
        
        try {
            con.setAutoCommit(false);
            
            statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeQuery();
            
            con.commit();
            elimino = true;
            
        } catch (SQLException ex) {
            try {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }finally{
            super.closeConection();
        }
        
        return elimino;
    }
    
    
    
    public LinkedList<Pedido> getPedidos(){

        LinkedList<Pedido> pedidos = new LinkedList<>();
        con = super.getConection();
        String sql = "CALL ex_selectPedido();";
        
        try {
            
            statement = con.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Pedido pedido = new Pedido();
                pedido.setId(result.getInt(1));
                pedido.setCliente(result.getString(2));
                pedido.setFecha(result.getString(3));
                //pedido.setDetalle(result.getArray(""))
                pedidos.add(pedido);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            //super.closeConection();
        }
        return pedidos;
    }
    
    public String getDetalle(int id){

        String salida = "";
        con = super.getConection();
        String sql = "CALL ex_selectDetalle(?);";
        
        try {
            
            statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                salida += "<tr>+\n" +
                    "<td>"+result.getString(1)+"</td>+\n" +
                    "<td>"+result.getString(2)+"</td>+\n" +
                    "<td>"+result.getString(3)+"</td>+\n" +
                "</tr>";
            }
        } catch (SQLException ex) {
            Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            //super.closeConection();
        }
        return salida;
    }
    
}
