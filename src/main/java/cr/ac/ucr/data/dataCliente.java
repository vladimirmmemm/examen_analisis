package cr.ac.ucr.data;

import cr.ac.ucr.dominio.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class dataCliente extends Conexion{
    
    private static Connection con;
    private static PreparedStatement statement;
    
    public boolean insertarCliente(Cliente cliente){
        boolean inserto = false;
        con = super.getConection();
        String sql = " CALL ex_insertarCliente(?, ?, ?, ?);";
        
        try {
            con.setAutoCommit(false);
            
            statement = con.prepareStatement(sql);
            statement.setString(1, cliente.getCedula());
            statement.setString(2, cliente.getNombre());
            statement.setString(3, cliente.getTelefono());
            statement.setString(4, cliente.getCorreo());
            statement.executeQuery();
            
            con.commit();
            inserto = true;
            
        } catch (SQLException ex) {
            try {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }finally{
            super.closeConection();
        }
        
        return inserto;
    }
    
    public boolean modificarCliente(Cliente cliente){
        boolean inserto = false;
        con = super.getConection();
        String sql = " CALL ex_modificarCliente(?, ?, ?, ?);";
        
        try {
            con.setAutoCommit(false);
            
            statement = con.prepareStatement(sql);
            statement.setString(1, cliente.getCedula());
            statement.setString(2, cliente.getNombre());
            statement.setString(3, cliente.getTelefono());
            statement.setString(4, cliente.getCorreo());
            statement.executeQuery();
            
            con.commit();
            inserto = true;
            
        } catch (SQLException ex) {
            try {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }finally{
            super.closeConection();
        }
        
        return inserto;
    }
    
    public boolean eliminarCliente(String cedula){
        boolean elimino = false;
        con = super.getConection();
        String sql = " CALL ex_eliminarCliente(?);";
        
        try {
            con.setAutoCommit(false);
            
            statement = con.prepareStatement(sql);
            statement.setString(1, cedula);
            statement.executeQuery();
            
            con.commit();
            elimino = true;
            
        } catch (SQLException ex) {
            try {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }finally{
            super.closeConection();
        }
        
        return elimino;
    }
    
    public LinkedList<Cliente> getClientes(){
        LinkedList<Cliente> list = new LinkedList<>();
        
        con = super.getConection();
        String sql = " CALL ex_selectCliente();";
        
        try {
            
            statement = con.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Cliente cliente = new Cliente();
                cliente.setCedula(result.getString(1));
                cliente.setNombre(result.getString(2));
                cliente.setTelefono(result.getString(3));
                cliente.setCorreo(result.getString(4));
                
                list.add(cliente);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dataProducto.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            super.closeConection();
        }
        
        return list;
    }
}
