/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.ucr.servlet;

import cr.ac.ucr.dominio.Cliente;
import cr.ac.ucr.dominio.Detalle;
import cr.ac.ucr.dominio.Pedido;
import cr.ac.ucr.dominio.Producto;
import cr.ac.ucr.negocio.LogicaPedido;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author crisc
 */
@WebServlet(name = "insertarPedido", urlPatterns = {"/insertarPedido"})
public class insertarPedido extends HttpServlet {
    
    private static final LogicaPedido logPedido = new LogicaPedido();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        try(PrintWriter out = response.getWriter()){
            
            String cedula = request.getParameter("cedula");
            String detalle = request.getParameter("detalle");
            
            String[] temp = detalle.split(",");
            LinkedList<Detalle> lista = new LinkedList<>();
            
            System.out.println("Entro");
            
            for(int i=0; i<temp.length; i++){
                String[] temp2 = temp[i].split("-");
                Detalle deta = new Detalle(new Producto(Integer.parseInt(temp2[0]), "", 0, 0), Integer.parseInt(temp2[1]));
                lista.add(deta);
            }
            
            Pedido pedido = new Pedido();
            pedido.setCliente(cedula);
            pedido.setDetalle(lista);
            
            String salida = "false";

            if(logPedido.insertarPedido(pedido)){
                salida = "true";
            }
            
            out.println(salida);
        }
    }

}
