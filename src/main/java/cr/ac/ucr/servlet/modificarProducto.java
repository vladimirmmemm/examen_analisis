/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.ucr.servlet;

import cr.ac.ucr.dominio.Producto;
import cr.ac.ucr.negocio.LogicaProducto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author crisc
 */
public class modificarProducto extends HttpServlet {
    
    private static final LogicaProducto logP = new LogicaProducto();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        response.setContentType("text/html;charset=UTF-8");
        try(PrintWriter out = response.getWriter()){
            
            int id = Integer.parseInt(request.getParameter("id"));
            String nombre = request.getParameter("nombre");
            int cantidad = Integer.parseInt(request.getParameter("cantidad"));
            int precio = Integer.parseInt(request.getParameter("precio"));

            Producto pro = new Producto(id, nombre, cantidad, precio);
            String salida = "false";

            if(logP.modificarProducto(pro)){
                salida = "true";
            }
            
            out.println(salida);
        }
    }

}
