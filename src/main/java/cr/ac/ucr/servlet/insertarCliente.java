/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.ucr.servlet;

import cr.ac.ucr.dominio.Cliente;
import cr.ac.ucr.negocio.LogicaCliente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author crisc
 */
public class insertarCliente extends HttpServlet {

    private static final LogicaCliente logC = new LogicaCliente();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        try(PrintWriter out = response.getWriter()){
            
            String cedula = request.getParameter("cedula");
            String nombre = request.getParameter("nombre");
            String telefono = request.getParameter("tel");
            String correo = request.getParameter("correo");
            
            Cliente cliente = new Cliente(cedula, nombre, telefono, correo);
            
            String salida = "false";

            if(logC.insertarCliente(cliente)){
                salida = "true";
            }
            
            out.println(salida);
        }
    }

}
