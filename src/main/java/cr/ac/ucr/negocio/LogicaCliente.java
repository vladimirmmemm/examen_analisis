package cr.ac.ucr.negocio;

import cr.ac.ucr.data.dataCliente;
import cr.ac.ucr.dominio.Cliente;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;

public class LogicaCliente {
    
    private static dataCliente dataC;
    
    public LogicaCliente(){
        dataC = new dataCliente();
    }
    
    public boolean insertarCliente(Cliente cliente){
        boolean insertar = false;
        
        if(validarCorreo(cliente.getCorreo())){
            if(cliente.getCedula().length() == 9 && validarNumero(cliente.getCedula())){
                if(cliente.getTelefono().length() == 8 && validarNumero(cliente.getTelefono())){
                    insertar = dataC.insertarCliente(cliente);
                }
            }
        }
        
        return insertar;
    }
    public boolean modificarCliente(Cliente cliente){
        boolean insertar = false;
        
        if(validarCorreo(cliente.getCorreo())){
            if(cliente.getCedula().length() == 9 && validarNumero(cliente.getCedula())){
                if(cliente.getTelefono().length() == 8 && validarNumero(cliente.getTelefono())){
                    insertar = dataC.modificarCliente(cliente);
                }
            }
        }
        
        return insertar;
    }
    public boolean eliminarCliente(String cedula){

        return dataC.eliminarCliente(cedula);
    }
    public JSONArray getListaClientes(){
        return new JSONArray(dataC.getClientes());
    }
    
    public boolean validarCorreo(String correo){
        boolean valido = false;
        
        Pattern Plantilla = Pattern.compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
        Matcher Resultado = Plantilla.matcher(correo);
        
        if(Resultado.find()==true){
            valido = true;
        }
        
        return valido;
    }
    
    public boolean validarNumero(String num){
        boolean valido = false;
        
        try {
            Integer.parseInt(num);
            valido = true;
	} catch (NumberFormatException nfe){
            //return false;
	}
        
        return valido;
    }
}
