package cr.ac.ucr.negocio;

import cr.ac.ucr.data.dataPedido;
import cr.ac.ucr.dominio.Detalle;
import cr.ac.ucr.dominio.Pedido;
import java.util.LinkedList;
import org.json.JSONArray;

public class LogicaPedido {
    
    private static dataPedido dataPedido;
    
    public LogicaPedido(){
        dataPedido = new dataPedido();
    }
    
    public boolean insertarPedido(Pedido pedido){
        boolean inserto = false;
        
        if(pedido.getDetalle().size() >= 1 && validarNumero(pedido.getCliente()) && validarCantLista(pedido.getDetalle())){
            inserto = dataPedido.insertarPedido(pedido);
        }
        
        return inserto;
    }

    public boolean eliminarPedido(int id){
        //validar id
        return dataPedido.eliminarPedidos(id);
    }
    
    public JSONArray getListaPedidos(){
        return new JSONArray(dataPedido.getPedidos());
    }
    
    public String getListaDetalles(int id){
        
        return dataPedido.getDetalle(id);
    }
    
    public boolean validarCantLista(LinkedList<Detalle> lista){
        boolean valido = true;
        
        for(Detalle deta: lista){
            if(deta.getCantidad() < 1 ){
                valido = false;
            }
        }
        
        return valido;
    }
    
    public boolean validarNumero(String num){
        boolean valido = false;
        
        try {
            Integer.parseInt(num);
            valido = true;
	} catch (NumberFormatException nfe){
            //return false;
	}
        
        return valido;
    }
}
