package cr.ac.ucr.negocio;

import cr.ac.ucr.data.dataProducto;
import cr.ac.ucr.dominio.Producto;
import org.json.JSONArray;

public class LogicaProducto {
    
    private static dataProducto dataPro;
    
    public LogicaProducto(){
        dataPro = new dataProducto();
    }
    
    public boolean insertarProducto(Producto pro){
        boolean inserto = false;
        
        if(pro.getCantidad_Producto() >= 0 && pro.getPrecio_Producto() >= 0){
            inserto = dataPro.insertarProducto(pro);
        }
        
        return inserto;
    }
    public boolean modificarProducto(Producto pro){
        boolean inserto = false;
        
        if(pro.getCantidad_Producto() >= 0 && pro.getPrecio_Producto() >= 0){
            inserto = dataPro.modificarProducto(pro);
        }
        
        return inserto;
    }
    public boolean eliminarProducto(int id){
        
        return dataPro.eliminarProducto(id);
    }
    public JSONArray getListaProductos(){

        return new JSONArray(dataPro.getProductos());
    }
    
}
